# CKAN Puppet Module (Fork from landcarereseach)

Author: Michael Speth spethm@landcareresearch.co.nz

## Fork

The goal of this fork is to make ckan a single purpose module by removing dependencies like Solr, PostgreSQL,..

## Limitations

Ubuntu (12.04, untested on 14.04)

Using puppet/nodejs => 0.8.0 requires puppet 3.7 minimum (issue with nested contains: PUP-1597)

## Development

The module is Free and Open Source Software. Its available on bitbucket.  Please fork!