# == Class: ckan::config
#
# Configuration supporting ckan
# details: http://docs.ckan.org/en/ckan-2.0/install-from-package.html
#
# === Parameters
# [*site_url*]
#   The site url of the ckan instance.  Defaults to localhost.
#
# [*site_title*]
#   The title of the ckan instance.  Defaults to localhost.
#
# [*site_description*]
#   This is for a description, or tag line for the site, as displayed in the
#   header of the CKAN web interface.
#
# [*site_intro*]
#   This is for an introductory text used in the default template's index page.
#
# [*site_about*]
#   Multiline string can be used by indenting lines.  The format is in
#   Markdown.
#
#   Its better to overload the snippet in home/snippets/about_text.html
#   because if this parameter is set, this is not automatically translated.
#
# [*site_logo*]
#   This sets the logo use din the title bar.
#
# [*plugins*]
#   Specifies which CKAN plugins are to be enabled.
#   Default: 'stats text_preview recline_preview'
#
# [*backup_dir*]
#   The location where backups are stored.
#   Default: /backup
#
# === Variables
#
# [*ckan_etc*]
#   The configuration directory.
#
# [*ckan_default*]
#   The default directory in the ckan directory.
#
# [*ckan_src*]
#   The ckan src directory.
#
# [*ckan_img_dir*]
#   The directory to install images.
#
# [*ckan_css_dir*]
#   The directory that contains the css files.
#
# [*ckan_storage_path*]
#   The directory that contains the storage (ie downloads).
#
# [*ckan_plugin_dir*]
#   The directory that contains files that builds the plugins entry in the
#   production ini file
#
# [*license_file*]
#   The name of the license file.
#
# [*ckan_conf*]
#   The default production ini file for ckan configuration.
#
# [*paster*]
#   The full path to the paster command.
#
class ckan::config (
  $site_url           = 'localhost',
  $site_title         = 'localhost',
  $site_description   = '',
  $site_intro         = '',
  $site_about         = '',
  $site_logo          = '',
  $plugins            = 'stats text_preview recline_preview',
  $backup_dir         = '/backup',
){


  anchor{'ckan::config::begin':}

  # Variables
  $ckan_data_dir = ['/var/lib/ckan',$ckan::ckan_storage_path]

  # CKAN configuration
  concat { $ckan::ckan_conf:
    owner   => root,
    group   => root,
    mode    => '0644',
    require => Anchor['ckan::config::begin'],
  }
  concat::fragment { 'config_head':
    target  => $ckan::ckan_conf,
    content => template('ckan/production_head.ini.erb'),
    order   => 01,
  }
  concat::fragment { 'config_tail':
    target  => $ckan::ckan_conf,
    content => template('ckan/production_tail.ini.erb'),
    order   => 99,
  }

  # write default plugins
  file {"${ckan::ckan_etc}/plugins":
    ensure  => directory,
    require => [Concat[$ckan::ckan_conf], Concat::Fragment['config_head',
    'config_tail']],
  }
  file {"${ckan::ckan_etc}/plugins/default":
    ensure  => file,
    content => inline_template("<%= scope.lookupvar('ckan::plugins') %>"),
    require => File["${ckan::ckan_etc}/plugins"],
    notify  => Exec['update_plugins'],
  }

  # add the logo but its set via the web ui and also set via production.ini
  # however, I'm not certain that the production.ini has any effect...
  if $site_logo != '' {
    file {"${ckan::ckan_img_dir}/site_logo.png":
      ensure  => file,
      source  => $site_logo,
      require => File["${ckan::ckan_etc}/plugins/default"],
      before  => File[$ckan_data_dir],
    }
  }

  file {$ckan_data_dir:
    ensure  => directory,
    owner   => www-data,
    group   => www-data,
    mode    => '0755',
    require => File["${ckan::ckan_etc}/plugins/default"],
  }

  file { "${ckan::ckan_default}/${ckan::license_file}":
    ensure  => file,
    source  => $ckan::license,
    mode    => '0644',
    require => File[$ckan_data_dir],
    before  => File['/usr/local/bin/ckan_create_admin.bash'],
  }

  if $ckan::custom_imgs != '' {
    # manage the default image directory
    ckan::custom_images { $ckan::custom_imgs:
      require => File[$ckan_data_dir],
      before  => File['/usr/local/bin/ckan_create_admin.bash'],
    }
  }

  # download custom css if specified
  if $ckan::custom_css != 'main.css' {
    file {"${ckan::ckan_css_dir}/custom.css":
      ensure  => file,
      source  => $ckan::custom_css,
      require => File[$ckan_data_dir],
      before  => File['/usr/local/bin/ckan_create_admin.bash'],
    }
  }

  # backup configuration
  if $ckan::enable_backup {
    if $ckan::backup_daily {
      $weekday = 'absent'
    }else {
      $weekday = '7'
    }
    file { $backup_dir:
      ensure  => directory,
      owner   => backup,
      group   => backup,
      mode    => '0755',
      require => File[$ckan_data_dir],
    }
    file { '/usr/local/bin/ckan_backup.bash':
      ensure  => file,
      source  => 'puppet:///modules/ckan/ckan_backup.bash',
      mode    => '0755',
      require => File[$backup_dir],
    }
    file { '/usr/local/bin/ckan_recover_db.bash':
      ensure  => file,
      source  => 'puppet:///modules/ckan/ckan_recover_db.bash',
      mode    => '0755',
      require => File['/usr/local/bin/ckan_backup.bash'],
    }
    # backup database either every day or every week at 5 am
    cron {'ckan_backup':
      command => '/usr/local/bin/ckan_backup.bash',
      user    => backup,
      minute  => '0',
      hour    => '5',
      weekday => $weekday,
      require => File['/usr/local/bin/ckan_recover_db.bash'],
      before  => File['/usr/local/bin/ckan_create_admin.bash'],
    }
  }

  # additional userful scripts
  file { '/usr/local/bin/ckan_create_admin.bash':
    ensure  => file,
    source  => 'puppet:///modules/ckan/ckan_create_admin.bash',
    mode    => '0755',
    require => File[$ckan_data_dir],
  }
  file { '/usr/local/bin/ckan_delete_account.bash':
    ensure  => file,
    source  => 'puppet:///modules/ckan/ckan_delete_account.bash',
    mode    => '0755',
    require => File['/usr/local/bin/ckan_create_admin.bash'],
  }
  file { '/usr/local/bin/ckan_upgrade_db.bash':
    ensure  => file,
    source  => 'puppet:///modules/ckan/ckan_upgrade_db.bash',
    mode    => '0755',
    require => File['/usr/local/bin/ckan_delete_account.bash'],
  }
  file { '/usr/local/bin/ckan_init_db.bash':
    ensure  => file,
    source  => 'puppet:///modules/ckan/ckan_init_db.bash',
    mode    => '0755',
    require => File['/usr/local/bin/ckan_upgrade_db.bash'],
  }
  file {'/usr/local/bin/ckan_create_views.bash':
    ensure  => file,
    content => template('ckan/ckan_create_views.bash.erb'),
    mode    => '0755',
    require => File['/usr/local/bin/ckan_init_db.bash'],
  }

  anchor{'ckan::config::end':
    require => File['/usr/local/bin/ckan_init_db.bash'],
  }
}
