# == Class: ckan::params
#
# The os specific parameters for ckan installs.
#
# === Parameters
#
# === Variables
#
# [*site_url*]
#   The url for the ckan site.
#
# [*param site_title*]
#   The title of the web site.
#
# [*site_description*]
#   The description (found in header) of the web site.
#
# [*site_intro*]
#   The introduction on the landing page.
#
# [*site_about*]
#   Information on the about page.
#
# [*plugins*]
#   Contains the ckan plugins to be used by the installation.
#
# [*app_instance_id*]
#   The secret password for the app instance .
#   Use paster make-config to generate a config file that contains
#   a new password.
#
# [*beaker_secret*]
#   The secret password for beaker
#   Use paster make-config to generate a config file that contains
#   a new password.
#
# [*site_logo*]
#   The source of the logo.  Should be spedified as
#   puppet:///<your module>/<image>.png
#   Note, should be a png file.
#
# [*license*]
#   The source to the json license file.  Should be specified as
#   puppet:///<your module>/<license file> and maintained by your module
#
# [*is_ckan_from_repo*]
#   A boolean to indicate if the ckan package should be installed through an
#   already configured repository setup outside of this module.
#   If using Ubuntu/Deb should be able to do "apt-get install python-ckan"
#   Its the same idea for yum and other package managers.
#
# [*ckan_version*]
#   Helps identify settings and configuration necessary between the different
#   version of ckan.
#   Valid format: '2.2', '2.3', etc.
#   Note, ckan_package_url & ckan_package_filename are not set,
#   than the ckan version will use the package url from ckan.org and the
#   appropriate name.
#   Default: undef
#
# [*ckan_package_url*]
#   If not using a repo, then this url needs to be
#   specified with the location to download the package.
#   Note, this is using dpkg so deb/ubuntu only.
#   Default: undef
#
# [*ckan_package_filename*]
#   The filename of the ckan package.
#   Default: undef
#
# [*custom_css*]
#   The source to a css file used for the ckan site.
#   This replaces the default main.css.  Should be specified as
#   puppet:///<your module>/<css filename> and maintained by your module.
#   Note, images used in the custom css should be set in custom_imgs.
#
# [*custom_imgs*]
#   An array of source for the images to be used by the css.
#   Should be specified as
#   ['puppet:///<your module>/<img1>','...']
#
# [*recaptcha_publickey*]
#   The public key for recaptcha (by default not set).
#
# [*recaptcha_privatekey*]
#   The private key for recaptcha (by default not set).
#
# [*max_resource_size*]
#   The maximum in megabytes a resource upload can be.
#
# [*datapusher_formats*]
#   File formats that will be pushed to the DataStore by the DataPusher.
#   When adding or editing a resource which links to a file in one of these
#   formats, the DataPusher will automatically try to import its contents
#   to the DataStore.
#
# [*default_views*]
#   Defines the resource views that should be created by default when creating
#   or updating a dataset. From this list only the views that are relevant to a
#   particular resource format will be created. This is determined by each
#   individual view.
#   Default: image_view recline_view
#
# ~~[*preview_loadable*]~~ **Depricated**
#   Defines the resource formats which should be loaded directly in an iframe
#   tag when previewing them if no Data Viewer can preview it.
#   Only used in CKAN version 2.2 and below.
#   As of CKAN 2.3, use default_views
#
# [*text_formats*]
#   Formats used for the text preview
#
# [*postgres_pass*]
#   The password for the postgres user of the database (admin user).
#
# [*ckan_pass*]
#   The password for the ckan user of the database.
#
# [*db_user*]
#   The username of the ckan user of the database (default: ckan_default)
#
# [*db_hostname*]
#   The url or ip address of the database server (default: localhost)
#
# [*db_name*]
#   The name of the database (default: ckan_default)
#
# [*datastore_user*]
#   The username of the datastore user (default: datastore_default)
#
# [*datastore_password*]
#   The password for the datastore user
#
# [*datastore_name*]
#   The name of the datastore database (default: datastore_default)
#
# [*datastore_hostname*]
#   The url of ip address of the datastore database server (default: localhost)
#
# [*pg_hba_conf_defaults*]
#   True if use the default hbas and false to configure your own.
#   This module uses postgresql so this setting informs the postgresql module
#   that the hba's should be handled outside of this module.
#   Requires your own hba configuration.
#
# [*install_ckanapi*]
#   Installs the ckan api if set to true.  Default is false.
#   Also installs a helper script in /usr/bin/ckan/ckanapi.bash which
#   launches ckanapi with the environment setup.
#   Additional information: https://github.com/ckan/ckanapi
#
# [*enable_backup*]
#   Backs up the database to /backup/ckan_database.pg_dump.
#   Default: true
#
# [*backup_daily*]
#   If backups enabled, sets backups to either daily (true) or weekly (false).
#   Default: true
#
# [*solr_schema_version*]
#   The version of the solr schema to use.
#   Valid options:
#   * '1.2'
#   * '1.3'
#   * '1.4'
#   * '2.0'
#   * 'spatial' - configures solar with the spatial extensions.
#   * 'default'
#   The options correspond to the following structure.
# /usr/lib/ckan/default/src/ckan/ckan/config/solr/schema-<solr_schema_version>
#   The only exception is default which means schema.xml (required as of
#   ckan 2.3).
#
# [*locale_default*]
#  Use this to specify the locale (language of the text) displayed in the
#  CKAN Web UI.
#  default: 'en'
#
# [*i18n_directory*]
#   By default, the locales are searched for in the ckan/i18n directory.
#   Use this option if you want to use another folder.
#   default: ''
#
# [*jts_url*]
#   The url to be used to download the jts library for solr spatial ext.
#   Default: uses version 1.13
#
# [*solr_hostname*]
#   The url or ip address to the Solr server (default: localhost)
#
# [*solr_port*]
#   The port used by the Solr application (default: 8983)
#
# === Examples
#
#
# === Copyright
#
# GPL-3.0+
#
class ckan::params {

  # variables
  $site_url               = 'localhost'
  $site_title             = 'localhost'
  $site_description       = ''
  $site_intro             = ''
  $site_about             = ''
  $plugins                = ''
  $app_instance_id        = ''
  $beaker_secret          = ''
  $site_logo              = ''
  # due to issue, have to supply https://github.com/ckan/ckan/issues/2110
  # our own license file
  $license                = 'puppet:///modules/ckan/license.json'
  $is_ckan_from_repo      = true
  $ckan_version           = undef
  $ckan_package_url       = undef
  $ckan_package_filename  = undef
  $custom_css             = 'main.css'
  $custom_imgs            = ''
  $recaptcha_version      = '2'
  $recaptcha_publickey    = undef
  $recaptcha_privatekey   = undef
  $max_resource_size      = 100
  $max_image_size         = 10
  $datapusher_formats     = 'csv xls application/csv application/vnd.ms-excel'
  $default_views          = 'image_view recline_view'
  $preview_loadable       =
    "html htm rdf+xml owl+xml xml n3 n-triples turtle plain atom csv\
 tsv rss txt json"
  $text_formats           = 'text plain text/plain'
  $json_formats           = 'json'
  $xml_formats            = 'xml rdf rss'
  $postgres_pass          = 'pass'
  $ckan_pass              = 'pass'
  $db_user                = 'ckan_default'
  $db_name                = 'ckan_default'
  $db_hostname            = 'localhost'
  $datastore_user         = 'datastore_default'
  $datastore_password     = 'pass'
  $datastore_name         = 'datastore_default'
  $datastore_hostname     = 'localhost'
  $pg_hba_conf_defaults   = true
  $pg_hba_rules           = undef
  $install_ckanapi        = false
  $enable_backup          = true
  $backup_daily           = true
  $solr_schema_version    = 'default'
  $solr_hostname          = 'localhost'
  $solr_port              = '8983'
  $locale_default         = 'en'
  $i18n_directory         = ''
  $display_timezone       = 'UTC'

  # additional variables imported from config
  $ckan_etc          = '/etc/ckan'
  $ckan_default      = "${ckan_etc}/default"
  $ckan_src          = '/usr/lib/ckan/default/src/ckan'
  $ckan_img_dir      = "${ckan_src}/ckan/public/base/images"
  $ckan_css_dir      = "${ckan_src}/ckan/public/base/css"
  $ckan_storage_path = '/var/lib/ckan/default'
  $ckan_plugin_dir   = "${ckan_etc}/plugins"
  $license_file      = 'license.json'
  $ckan_conf         = "${ckan_default}/production.ini"
  $paster            = '/usr/lib/ckan/bin/paster'

  # used for setting jts for spatial search
  $jts_base_url =
  'http://search.maven.org/remotecontent?filepath=com/vividsolutions/jts/'
  $jts_1_13  = "${jts_base_url}/1.13/jts-1.13.jar"
  $jts_1_12  = "${jts_base_url}/1.12/jts-1.12.jar"
  $jts_1_11  = "${jts_base_url}/1.11/jts-1.11.jar"
  $jts_url   = $jts_1_13

  # OS Specific configuration
  case $::osfamily {

      'redhat': {
        $required_packages = ['epel-release','httpd', 'mod-wsgi', 'nginx',
                              'libpqxx-devel','python-paste-script']
        # wsgi specific configuration
        $wsgi_command = '/usr/sbin/a2enmod wsgi'
        $wsgi_creates = '/etc/httpd/mods-enabled/wsgi.conf'
        $apache_server = 'httpd'
      }

      'debian':{
        $required_packages = ['apache2', 'libapache2-mod-wsgi', 'nginx',
                              'libpq5', 'python-pastescript']
        # wsgi specific configuration
        $wsgi_command = '/usr/sbin/a2enmod wsgi'
        $wsgi_creates = '/etc/apache2/mods-enabled/wsgi.conf'
        $apache_service = 'apache2'
      }

      default: {
        fail("Unsupported OS ${::osfamily}.  Please use a debian or \
redhat based system")
      }
  }
}
