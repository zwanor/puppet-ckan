# == Class: ckan
#
# Installs, configures, and manages ckan.
# Install Details: http://docs.ckan.org/en/ckan-2.0/install-from-package.html
#
# Additional features:
# * Database can be backed up to /backup/ckan_database.pg_dump determined
#   by the frequency selected.
#
# === Parameters
#
# [*site_url*]
#   The url for the ckan site.
#
# [*param site_title*]
#   The title of the web site.
#
# [*site_description*]
#   The description (found in header) of the web site.
#
# [*site_intro*]
#   The introduction on the landing page.
#
# [*site_about*]
#   Information on the about page.
#
# [*plugins*]
#   Contains the ckan plugins to be used by the installation.
#
# [*app_instance_id*]
#   The secret password for the app instance .
#   Use paster make-config to generate a config file that contains
#   a new password.
#
# [*beaker_secret*]
#   The secret password for beaker
#   Use paster make-config to generate a config file that contains
#   a new password.
#
# [*site_logo*]
#   The source of the logo.  Should be spedified as
#   puppet:///<your module>/<image>.png
#   Note, should be a png file.
#
# [*license*]
#   The source to the json license file.  Should be specified as
#   puppet:///<your module>/<license file> and maintained by your module
#
# [*ckan_version*]
#   Helps identify settings and configuration necessary between the different
#   version of ckan.
#   Valid format: '2.2', '2.3', etc.
#   Note, ckan_package_url & ckan_package_filename are not set,
#   than the ckan version will use the package url from ckan.org and the
#   appropriate name.
#   Default: undef
#
# [*is_ckan_from_repo*]
#   A boolean to indicate if the ckan package should be installed through an
#   already configured repository setup outside of this module.
#   If using Ubuntu/Deb should be able to do "apt-get install python-ckan"
#   Its the same idea for yum and other package managers.
#
# [*ckan_package_url*]
#   If not using a repo, then this url needs to be
#   specified with the location to download the package.
#   Note, this is using dpkg so deb/ubuntu only.
#
# [*ckan_package_filename*]
#   The filename of the ckan package.
#
# [*custom_css*]
#   The source to a css file used for the ckan site.
#   This replaces the default main.css.  Should be specified as
#   puppet:///<your module>/<css filename> and maintained by your module.
#   Note, images used in the custom css should be set in custom_imgs.
#
# [*custom_imgs*]
#   An array of source for the images to be used by the css.
#   Should be specified as
#   ['puppet:///<your module>/<img1>','...']
#
# [*recaptcha_version*]
#   The version of recaptcha.
#   Valid options:
#     - 1 Older style with a red box.  The user enters text
#     - 2 Newer style that the user clicks on a checkbox (cleaner).
#   Default: 2
#
# [*recaptcha_publickey*]
#   The public key for recaptcha (by default not set).
#
# [*recaptcha_privatekey*]
#   The private key for recaptcha (by default not set).
#
# [*max_resource_size*]
#   The maximum in megabytes a resource upload can be.
#   Default: 100
#
# [*max_image_size*]
#  The maximum in megabytes an image upload can be.
#  Default: 10
#
# [*datapusher_formats*]
#   File formats that will be pushed to the DataStore by the DataPusher.
#   When adding or editing a resource which links to a file in one of these
#   formats, the DataPusher will automatically try to import its contents
#   to the DataStore.
#
# [*default_views*]
#   Defines the resource views that should be created by default when creating
#   or updating a dataset. From this list only the views that are relevant to a
#   particular resource format will be created. This is determined by each
#   individual view.
#   Default: image_view recline_view
#
# ~~[*preview_loadable*]~~ **Depricated**
#   Defines the resource formats which should be loaded directly in an iframe
#   tag when previewing them if no Data Viewer can preview it.
#   Only used in CKAN version 2.2 and below.
#   As of CKAN 2.3, use default_views
#
# [*text_formats*]
#   Formats used for the text preview.
#   Default: 'text plain text/plain'
#
# [*json_formats*]
#   JSON based resource formats that will be rendered by the Text view plugin
#   Default: 'json'
#
# [*xml_formats*]
#   XML based resource formats that will be rendered by the Text view plugin
#   Default: 'xml rdf rss'
#
# [*postgres_pass*]
#   The password for the postgres user of the database (admin user).
#
# [*ckan_pass*]
#   The password for the ckan user of the database.
#
# [*db_user*]
#   The username of the ckan user of the database (default: ckan_default)
#
# [*db_hostname*]
#   The url or ip address of the database server (default: localhost)
#
# [*db_name*]
#   The name of the database (default: ckan_default)
#
# [*datastore_user*]
#   The username of the datastore user (default: datastore_default)
#
# [*datastore_password*]
#   The password for the datastore user
#
# [*datastore_name*]
#   The name of the datastore database (default: datastore_default)
#
# [*datastore_hostname*]
#   The url of ip address of the datastore database server (default: localhost)
#
# [*pg_hba_conf_defaults*]
#   True if use the default hbas and false to configure your own.
#   This module uses postgresql so this setting informs the postgresql module
#   that the hba's should be handled outside of this module.
#   Requires your own hba configuration.
#
# [*pg_hba_rules*]
#   This is a hash which uses create_resources to create the hba rules.
#   Optional.  The hba rules can also be defined outside of this class.
#   Default: undef
#
# [*install_ckanapi*]
#   Installs the ckan api if set to true.  Default is false.
#   Also installs a helper script in /usr/bin/ckan/ckanapi.bash which
#   launches ckanapi with the environment setup.
#   Additional information: https://github.com/ckan/ckanapi
#
# [*enable_backup*]
#   Backs up the database to /backup/ckan_database.pg_dump.
#   Default: true
#
# [*backup_daily*]
#   If backups enabled, sets backups to either daily (true) or weekly (false).
#   Default: true
#
#
# [*solr_schema_version*]
#   The version of the solr schema to use.
#   Valid options:
#   * '1.2'
#   * '1.3'
#   * '1.4'
#   * '2.0'
#   * 'spatial' - configures solar with the spatial extensions.
#                 Only supports bounding box.
#   * 'spatial-ext' - configures solar with the extended spatial extension.
#                     This allows for bounding box, point, and Polygon.
#   * 'default'
#   The options correspond to the following structure.
# /usr/lib/ckan/default/src/ckan/ckan/config/solr/schema-<solr_schema_version>
#   The only exception is default which means schema.xml (required as of
#   ckan 2.3).
#
# [*jts_url*]
#   The url to be used to download the jts library for solr spatial ext.
#   Only used if spatial-ext option is set.
#   Default: uses version 1.13
#
# [*solr_hostname*]
#   The url or ip address to the Solr server (default: localhost)
#
# [*solr_port*]
#   The port used by the Solr application (default: 8983)
#
# [*locale_default*]
#  Use this to specify the locale (language of the text) displayed in the
#  CKAN Web UI.
#  default: 'en'
#
# [*i18n_directory*]
#   By default, the locales are searched for in the ckan/i18n directory.
#   Use this option if you want to use another folder.
#   default: ''
#
# [*ckan_storage_path*]
#  The location where files will be stored for the file store.
#  Note, this module handles creating the directory; however, ensure the
#  path leading up to the directory has already been created.
#  Default: '/var/lib/ckan/default'
#
# [*display_timezone*]
#   By default, all datetimes are considered to be in the UTC timezone. 
#   Use this option to change the displayed dates on the frontend.
#   Internally, the dates are always saved as UTC.
#   This option only changes the way the dates are displayed. 
#   The valid values for this options 
#   [can be found at pytz](http://pytz.sourceforge.net/#helpers) 
#   Available from CKAN 2.5+ (has no effect on previous versions).
#   Default: UTC
#
# === Examples
#
#class { 'ckan':
#  site_url              => 'test.ckan.com',
#  site_title            => 'CKAN Test',
#  site_description      => 'A shared environment for managing Data.',
#  site_intro            => 'A CKAN test installation',
#  site_about            => 'Pilot data catalogue and repository.',
#  plugins               =>
#   'stats text_preview recline_preview datastore resource_proxy pdf_preview',
#  is_ckan_from_repo     => 'false',
#  ckan_package_url      =>
#   'http://packaging.ckan.org/python-ckan_2.1_amd64.deb',
#  ckan_package_filename => 'python-ckan_2.1_amd64.deb',
#}
#
# === Authors
#
# Michael Speth <spethm@landcareresearch.co.nz>
#
# === Copyright
#
# GPL-3.0+
#
class ckan (
  $site_url               = $ckan::params::site_url,
  $site_title             = $ckan::params::site_title,
  $site_description       = $ckan::params::site_description,
  $site_intro             = $ckan::params::site_intro,
  $site_about             = $ckan::params::site_about,
  $plugins                = $ckan::params::plugins,
  $app_instance_id        = $ckan::params::app_instance_id,
  $beaker_secret          = $ckan::params::beaker_secret,
  $site_logo              = $ckan::params::site_logo,
  $license                = $ckan::params::license,
  $is_ckan_from_repo      = $ckan::params::is_ckan_from_repo,
  $ckan_package_url       = $ckan::params::ckan_package_url,
  $ckan_package_filename  = $ckan::params::ckan_package_filename,
  $ckan_version           = $ckan::params::ckan_version,
  $custom_css             = $ckan::params::custom_css,
  $custom_imgs            = $ckan::params::custom_imgs,
  $recaptcha_version      = $ckan::params::recaptcha_version,
  $recaptcha_publickey    = $ckan::params::recaptcha_publickey,
  $recaptcha_privatekey   = $ckan::params::recaptcha_privatekey,
  $max_resource_size      = $ckan::params::max_resource_size,
  $max_image_size         = $ckan::params::max_image_size,
  $datapusher_formats     = $ckan::params::datapusher_formats,
  $default_views          = $ckan::params::default_views,
  $preview_loadable       = $ckan::params::preview_loadable,
  $text_formats           = $ckan::params::text_formats,
  $json_formats           = $ckan::params::json_formats,
  $xml_formats            = $ckan::params::xml_formats,
  $postgres_pass          = $ckan::params::postgres_pass,
  $ckan_pass              = $ckan::params::ckan_pass,
  #####
  $db_user                = $ckan::params::db_user,
  $db_name                = $ckan::params::db_name,
  $db_hostname            = $ckan::params::db_hostname,
  $datastore_user         = $ckan::params::datastore_user,
  $datastore_password     = $ckan::params::datastore_password,
  $datastore_name         = $ckan::params::datastore_name,
  $datastore_hostname     = $ckan::params::datastore_hostname,
  #####
  $pg_hba_conf_defaults   = $ckan::params::pg_hba_conf_defaults,
  $pg_hba_rules           = $ckan::params::pg_hba_rules,
  $install_ckanapi        = $ckan::params::install_ckanapi,
  $enable_backup          = $ckan::params::enable_backup,
  $backup_daily           = $ckan::params::backup_daily,
  $solr_schema_version    = $ckan::params::solr_schema_version,
  $solr_jts_ulr           = $ckan::params::jts_url,
  ####
  $solr_hostname          = $ckan::params::solr_hostname,
  $solr_port              = $ckan::params::solr_port,
  ####
  $locale_default         = $ckan::params::locale_default,
  $i18n_directory         = $ckan::params::i18n_directory,
  $ckan_storage_path      = $ckan::params::ckan_storage_path,
  $display_timezone       = $ckan::params::display_timezone,
) inherits ckan::params{
  notice("=== site_url = ${site_url}")
  # Check supported operating systems
  if $::osfamily != 'debian' {
    fail("Unsupported OS ${::osfamily}.  Please use a debian based system")
  }
#  case $::osfamily {
#      'redhat','debian':{ }
#      default: {
#        fail("Unsupported OS ${::osfamily}.  Please use a debian or \
#redhat based system")
#      }
#  }

  File {
    owner => root,
    group => root,
  }

  $ckan_package_dir = '/usr/local/ckan'

  anchor { 'ckan::begin':
    #notify => Class['ckan::service'],
  }
  class { 'ckan::install':
    #notify  => Class['ckan::service'],
    require => Anchor['ckan::begin'],
  }
#  class { 'ckan::db_config':
#    #notify  => Class['ckan::service'],
#    require => Class['ckan::install'],
#  }
  class { 'ckan::config':
    site_url         => $ckan::site_url,
    site_title       => $ckan::site_title,
    site_description => $ckan::site_description,
    site_intro       => $ckan::site_intro,
    site_about       => $ckan::site_about,
    site_logo        => $ckan::site_logo,
    plugins          => $ckan::plugins,
    #notify           => Class['ckan::service'],
    #subscribe        => Class['ckan::install'],
    #require          => Class['ckan::db_config'],
    require          => Class['ckan::install'],
  }
  class { 'ckan::service':
    #subscribe => Class['ckan::install','ckan::config','ckan::db_config'],
    subscribe => Class['ckan::config'],
  }

  class { 'ckan::postinstall':
    require => Class['ckan::service'],
  }
  if $install_ckanapi {
    class {'ckan::ckanapi':
      require => Class['ckan::postinstall'],
      before  => Anchor['ckan::end'],
    }
  }
  anchor { 'ckan::end':
    require => Class['ckan::postinstall'],
  }
}
