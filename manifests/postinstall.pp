# == Class ckan::postinstall
#
# Manages tasks to be performed after the initial install like
# initializing the database
#
# details: http://docs.ckan.org/en/ckan-2.0/install-from-package.html
#
class ckan::postinstall {

  anchor{'ckan::postinstall::begin':}

  # variables
#  case $ckan::ckan_version {
#    '2.4': {
#      $set_database_command = "/usr/bin/sudo ckan datastore set-permissions |
#/usr/bin/sudo -u postgres psql --set ON_ERROR_STOP=1"
#    }
#    '2.3': {
#      $set_database_command = "/usr/bin/sudo ckan datastore set-permissions |
#/usr/bin/sudo -u postgres psql --set ON_ERROR_STOP=1"
#    }
#    default: {
#      $set_database_command =
#"/usr/bin/python /usr/lib/ckan/default/src/ckan/ckanext/\
#datastore/bin/datastore_setup.py ckan_default datastore_default ckan_default\
# ckan_default datastore_default -p postgres"
# 
#    }
#  }

  check_run::task { 'init_db':
    exec_command => '/usr/local/bin/ckan_init_db.bash',
    require      => Anchor['ckan::postinstall::begin'],
    #before       => Check_run::Task['set_database_perms'],
  }

#  check_run::task { 'set_database_perms':
#    exec_command => $set_database_command,
#    #require      => Check_run::Task['init_db'],
#    require      => Anchor['ckan::postinstall::begin'],
#  }

  anchor{'ckan::postinstall::end':
    require => Check_run::Task['init_db'],
  }
}
