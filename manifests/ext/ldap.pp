# == Class: ckan::ext::ldap
#
# Installs the "ldap" extension.
#
# === Parameters
#
# [*uri*]
#   The uri to the ldap server to connect.
#   Example: 'ldap://localhost:389'
#
# [*base_dn*]
#   The ldap base dn to use for user authentication.
#   Example: 'ou=users,dc=landcareresearch,dc=co,dc=nz'
#
# [*search_filter*]
#   The filter for searching through identities in ldap.
#   Default: 'uid={login}'
#
# [*username*]
#   The user name to use as a lookup.
#   Default: 'uid'
#
# [*email*]
#   The field that contains the user's email address.
#   Default: 'email'
#
# [*fullname*]
#   The field that contains the user's full name.
#   Default: cn'
#
# [*organization_role*]
#   The role of the user when logged in through ldap.
#   Default: member
#
# [*organization_id*]
# If this is set, users that log in using LDAP will automatically get added
# to the given organization.
#
# To create the organisation specified in ckanext.ldap.organization.id use
# the paste command:
# paster --plugin=ckanext-ldap ldap setup-org -c
# /etc/ckan/default/development.ini
#
#
class ckan::ext::ldap (
  $uri,
  $base_dn,
  $organization_id,
  $search_filter     = 'uid={login}',
  $username          = 'uid',
  $email             = 'mail',
  $fullname          = 'cn',
  $organization_role = 'member',
  ){
  # required packages for ldap integration
  $required_packages = ['libldap2-dev','libsasl2-dev','libssl-dev','python-dev']
  ensure_packages($required_packages)

  ckan::ext { 'ldap':
    source           => 'git://github.com/okfn/ckanext-ldap.git',
    revision         => 'master',
    plugin           => 'ldap',
    pip_requirements => 'requirements.txt',
    require          => Package[$required_packages],
  }
  # set configuration
  concat::fragment { 'ckanext-ldap':
    target  => '/etc/ckan/default/production.ini',
    content => "
# LDAP Extension
ckanext.ldap.uri = ${uri}
ckanext.ldap.base_dn = ${base_dn}
ckanext.ldap.search.filter = ${search_filter}
ckanext.ldap.username = ${username}
ckanext.ldap.email = ${email}
ckanext.ldap.fullname = ${fullname}
ckanext.ldap.organization.id = ${organization_id}
ckanext.ldap.organization.role = ${organization_role}
",
  }
}
