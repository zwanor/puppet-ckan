# == Class: ckan::ext::newzealand
#
# Installs the Landcare Research extension.
#
# === Requirements
#  Installs the following extensions.
# * ckan::ext::repeating
# * ckan::ext::ldap
#
# === Parameters
#
# [*uri*]
#   The uri to the ldap server to connect.
#   Example: 'ldap://localhost:389'
#
# [*base_dn*]
#   The ldap base dn to use for user authentication.
#   Example: 'ou=users,dc=landcareresearch,dc=co,dc=nz'
#
# [*organization_id*]
# If this is set, users that log in using LDAP will automatically get added
# to the given organization.
#
class ckan::ext::lcrnz (
  $uri,
  $base_dn,
  $organization_id,
  ){
  class {'ckan::ext::repeating':
  }
  class {'ckan::ext::ldap':
    uri             => $uri,
    base_dn         => $base_dn,
    organization_id => $organization_id,
  }
  ckan::ext { 'lcrnz':
    source   => 'git://github.com/okfn/ckanext-lcrnz.git',
    revision => 'master',
    plugin   => 'lcrnz',
  }
## This is the id of the Landcare Research CKAN organization
## You can get it from http://datastore.landcareresearch.co.nz/api/action/organization_show?id=landcare-research
#ckanext.ldap.organization.id = cfa490ed-d9e4-46a6-8c43-a7057cfebd1c
#ckanext.ldap.organization.role = member
#
#
## Custom i18n folder (for replacing "organizations" with "collections")
## Update the path if necessary
#ckan.locale_default = en_NZ
#ckan.i18n_directory = /usr/lib/ckan/default/src/ckanext-lcrnz
}
