#!/bin/bash
# initializes the database

. /usr/lib/ckan/default/bin/activate
cd /usr/lib/ckan/default/src/ckan
paster db upgrade --config=/etc/ckan/default/production.ini
