# -- main stage --
class { 'ckan':
  site_title            => 'CKAN Test',
  site_description      => 'A shared environment for managing Data.',
  site_intro            => 'A CKAN test installation',
  site_about            => 'Pilot data catalogue and repository.',
  plugins               =>
    'stats text_preview recline_preview datastore resource_proxy pdf_preview',
  app_instance_id       => 'pass',
  beaker_secret         => 'pass',
  is_ckan_from_repo     => false,
  ckan_package_url      =>
    'http://packaging.ckan.org/python-ckan_2.2_amd64.deb',
  ckan_package_filename => 'python-ckan_2.2_amd64.deb',
  max_resource_size     => '1000',
  solr_url              =>
  'http://repository.test.zen.landcareresearch.co.nz/download/solr/',
  solr_version          => '4.10.3',
}